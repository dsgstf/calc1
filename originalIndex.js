function addToInput(val) {
	if ($("#bar").val() === undefined) {
		console.log("Setting text to be "  + val);
		$("#bar").val(val);
	}
	else {
		$("#bar").val($("#bar").val() + val);		
	}
}


function clear() {
	console.log("Clear method called");
	$("#bar").val("");
}

$(document).ready(function() {
	$("#clear").on("click", clear);
	$("#equals").on("click", function() {
		var queryStr = $("#bar").val();
		var result = eval(queryStr.substring(0, queryStr.length));
		clear();
		addToInput(result);
	});
});



